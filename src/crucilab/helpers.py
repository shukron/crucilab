import requests


class Crucible(object):
    """
    Crucible API helper class
    """

    def __init__(self, server):
        self.server = server

    CRUCIBLE_HEADERS = {
        "Accept": "application/json"
    }

    REVIEWS_FOR_ISSUE_PATH = "/rest-service/search-v1/reviewsForIssue"
    REPO_DETAILS_PATH = "/rest-service/repositories-v1/{repo_name}"
    REVIEW_TRACKED_BRANCHES_PATH = \
        "/rest/branchreview/latest/trackedbranch/{review_id}"

    def request(self, method="GET", path="", **kwargs):
        user_headers = kwargs.pop("headers", {})
        headers = self.CRUCIBLE_HEADERS.copy()
        headers.update(user_headers)
        return requests.request(
            method, "{}{}".format(self.server, path),
            headers=headers, **kwargs
        )

    def get_related_reviews(self, issue_key, exclude_closed=True):
        """
        Get a list of all the reviews related to a JIRA issue.
        :param issue_key: The JIRA issue key.
        :param exclude_closed: Do not include closed reviews in the result.

        :return list: A list of reviews
        """
        response = self.request(
            path=self.REVIEWS_FOR_ISSUE_PATH,
            params=dict(jiraKey=issue_key)
        )
        return [r for r in response.json()["reviewData"]
                if r["state"] != "Closed" or not exclude_closed]

    def get_repo_location(self, repo_name):
        """
        Get the repo location (URL)

        :param repo_name: The repo name on FishEye
        :return: The URL of the repo location
        """
        response = self.request(
            path=self.REPO_DETAILS_PATH.format(repo_name=repo_name)
        )
        data = response.json()
        return data["location"]

    def get_branches_in_review(self, review_id):
        """
        Get a list of all the branches in a review

        :param review_id: Crucible review ID, like CR-1
        :return: A list of branch details
        """
        response = self.request(
            path=self.REVIEW_TRACKED_BRANCHES_PATH.format(review_id=review_id)
        )
        return response.json()

    def get_branches_for_issue(self, issue_key):
        reviews = [r["permaId"]["id"]
                   for r in self.get_related_reviews(issue_key)]
        branches = []
        for review in reviews:
            branches.extend(self.get_branches_in_review(review))
        return branches


class Crucilab(object):
    """
    Main class for this custom server's logic
    """

    def __init__(self, logger, jira_client, gitlab_client, crucible):
        self.logger = logger
        self._jira = jira_client
        self._gitlab = gitlab_client
        self._crucible = crucible

    def find_gitlab_project(self, project_name, git_url):
        """
        Find a gitlab project based on its name and git URL
        The Gitlab API does not provide a way to get a project by its git URL,
        which is unique.
        The only way to search project is by name. Because of that we first
        search by name and then compare URLs. The first project with matching
        URL is returned.

        :param project_name: The project name in Crucible
        :param git_url: The git URL of the repo.
        :return: Gitlab Project object.
        """
        projects = self._gitlab.projects.list(search=project_name)
        for p in projects:
            if p.ssh_url_to_repo == git_url:
                return p
        return None

    def create_merge_request(self, issue_key, title, repo_name, source_branch,
                             target_branch):
        """
        Create a new Merge Request.

        :param issue_key: The issue triggering this merge request
        :param title: The title for the merge request
        :param repo_name: The Crucible name of the repo in which the MR occurs.
        :param source_branch: The name of the branch to be merged
            (the "feature branch")
        :param target_branch: The name of the branch to be merged into.
            (the "base branch". Usually "master" but not necessarily)
        :return: The newly created merge request.
        """
        repo_url = self._crucible.get_repo_location(repo_name)
        project = self.find_gitlab_project(repo_name, repo_url)
        if project is None:
            msg = "Could not find Gitlab project with name {}." \
                  " Therefor, no merge request was opened for branch {}." \
                  " Make sure that the repo name matches in both Gitlab and" \
                  " Crucible.".format(repo_name, source_branch)
            self._jira.add_comment(issue_key, msg)
            raise AttributeError(msg)

        mr = project.mergerequests.create(data=dict(
            source_branch=source_branch, target_branch=target_branch,
            title=title, remove_source_branch=True
        ))

        msg = "Merge request created for {}/{} at {}".format(
            repo_name, source_branch, mr.web_url
        )
        self._jira.add_comment(issue_key, msg)
        return mr

    def create_merge_requests_for_issue(self, issue_key):
        issue = self._jira.issue(issue_key)
        self.logger.debug("Issue %s moved to 'In Review' state", issue_key)
        branches = self._crucible.get_branches_for_issue(issue_key)
        title = "{}: {}".format(issue_key, issue.fields.summary)
        for branch in branches:
            repo_name = branch["repositoryName"]
            branch_name = branch["reviewedBranch"]
            target_branch = branch["baseBranch"]
            try:
                mr = self.create_merge_request(
                    issue_key, title, repo_name, branch_name, target_branch)
            except Exception as e:
                self.logger.exception("Error creating merge request: %s", e)
                continue
            self.logger.info("Merge request created at %s", mr.web_url)
