from flask import Flask
from gitlab import Gitlab
from jira import JIRA

from crucilab.settings import Settings
from .helpers import Crucilab, Crucible

app = Flask(__name__)

_crucilab = None


@app.route("/webhooks/<issue_key>/review_started", methods=["POST"])
def on_review_start(issue_key):
    _crucilab.create_merge_requests_for_issue(issue_key)
    return "All done!"


def start_server(env_file):
    Settings.load(env_file)

    crucible = Crucible(Settings.CRUCIBLE_SERVER)
    jira_client = JIRA(
        server=Settings.JIRA_SERVER,
        basic_auth=(Settings.JIRA_USER,
                    Settings.JIRA_PASS)
    )
    gitlab_client = Gitlab(
        Settings.GITLAB_SERVER,
        private_token=Settings.GITLAB_PRIVATE_TOKEN,
        api_version="4"
    )
    global _crucilab
    _crucilab = Crucilab(app.logger, jira_client, gitlab_client, crucible)
    app.run(host=Settings.HOST, port=Settings.PORT, debug=Settings.DEBUG)
