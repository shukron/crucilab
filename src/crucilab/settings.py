import os

from dotenv import load_dotenv

STR_BOOL = {
    "1": True,
    "true": True,
    "True": True,
    "0": False,
    "false": False,
    "False": False
}


def to_bool(value):
    if isinstance(value, bool):
        return value
    if isinstance(value, (int, float)):
        return bool(value)
    if value not in STR_BOOL:
        raise ValueError(
            "Cannot convert {} to bool. Valid values are {}".format(
                value, STR_BOOL.keys()
            ))
    return STR_BOOL[value]


class Settings(object):

    HOST = "localhost"
    PORT = 8888
    DEBUG = False
    JIRA_SERVER = None
    JIRA_USER = None
    JIRA_PASS = None
    GITLAB_SERVER = None
    GITLAB_PRIVATE_TOKEN = None
    CRUCIBLE_SERVER = None

    @classmethod
    def load(cls, env_file):
        if env_file is not None:
            load_dotenv(env_file, verbose=True)
        cls.apply_settings()

    @classmethod
    def apply_settings(cls):
        cls.HOST = cls.get_env("HOST", cls.HOST)
        cls.PORT = int(cls.get_env("PORT", cls.PORT))
        cls.DEBUG = to_bool(cls.get_env("DEBUG", cls.DEBUG))
        cls.JIRA_SERVER = cls.get_env("JIRA_SERVER")
        cls.JIRA_USER = cls.get_env("JIRA_USER")
        cls.JIRA_PASS = cls.get_env("JIRA_PASS")
        cls.GITLAB_SERVER = cls.get_env("GITLAB_SERVER")
        cls.GITLAB_PRIVATE_TOKEN = cls.get_env("GITLAB_PRIVATE_TOKEN")
        cls.CRUCIBLE_SERVER = cls.get_env("CRUCIBLE_SERVER")

    @classmethod
    def get_env(cls, key, default=None):
        v = os.environ.get(key, default)
        if v is None:
            raise EnvironmentError(
                "Missing environment variable: {}. Either add it to the a .env"
                " file or set it manually".format(key))
        return v
