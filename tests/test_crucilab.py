import json
from urllib import parse

from mock import patch, Mock, ANY, call
from requests.models import Response
from pytest import raises, fixture

from crucilab.helpers import Crucible, Crucilab

from . import files


CRUCIBLE_DUMMY_DATA = {
    Crucible.REVIEWS_FOR_ISSUE_PATH: "sof-10_reviews.json",
    Crucible.REPO_DETAILS_PATH.format(repo_name="my-repo"): "my-repo.json",
    Crucible.REVIEW_TRACKED_BRANCHES_PATH.format(review_id="CR-1"):
        "cr-1_branches.json",
    Crucible.REVIEW_TRACKED_BRANCHES_PATH.format(review_id="CR-2"):
        "cr-2_branches.json",
}
DUMMY_CRUCIBLE_SERVER = "http://crucible.com"


def find_file_for_path(path):
    for key, filename in CRUCIBLE_DUMMY_DATA.items():
        if path.startswith(key):
            return filename
    raise KeyError("No data file for %s" % (path,))


def read_json(file_name):
    return json.loads(files.read(file_name))


def mock_request(method, url, **kwargs):
    url = parse.urlparse(url)
    assert url.netloc == "crucible.com"

    file_name = find_file_for_path(url.path)

    res = Mock(spec=Response)
    res.json.return_value = read_json(file_name)
    return res


@fixture()
def gitlab_mock():
    projects = [Mock() for _ in range(10)]
    for p in projects:
        p.ssh_url_to_repo = "this is not the repo you are looking for"
        p.name = "not your repo"

    gitlab_mock = Mock()
    gitlab_mock.projects.list.return_value = projects
    return gitlab_mock


@patch("crucilab.helpers.requests.request", side_effect=mock_request)
def test_get_related_reviews(request):
    c = Crucible(DUMMY_CRUCIBLE_SERVER)
    reviews = c.get_related_reviews("SOF-10")
    request.assert_called_once_with(
        "GET", ANY,
        headers=ANY, params={"jiraKey": "SOF-10"}
    )
    assert reviews == read_json("sof-10_reviews.json")["reviewData"]


@patch("crucilab.helpers.requests.request", side_effect=mock_request)
def test_get_branches_for_issue(request):
    c = Crucible(DUMMY_CRUCIBLE_SERVER)
    branches = c.get_branches_for_issue("SOF-10")

    cr1_branches = read_json("cr-1_branches.json")
    cr2_branches = read_json("cr-2_branches.json")

    assert branches == cr1_branches + cr2_branches


@patch("crucilab.helpers.requests.request", side_effect=mock_request)
def test_get_repo_location(request):
    c = Crucible(DUMMY_CRUCIBLE_SERVER)
    location = c.get_repo_location("my-repo")
    assert location == "git@localhost:root/my-repo.git"


def test_find_gitlab_project(gitlab_mock):
    c = Crucilab(Mock(), Mock(), gitlab_mock, Mock())
    found = c.find_gitlab_project("my-repo", "git@localhost:root/my-repo.git")
    gitlab_mock.projects.list.assert_called_once_with(search="my-repo")
    assert found is None

    # Give one repo the matching git URL
    some_proj = gitlab_mock.projects.list.return_value[8]
    some_proj.ssh_url_to_repo = "git@localhost:root/my-repo.git"
    some_proj.name = "my-repo"
    found = c.find_gitlab_project("my-repo", "git@localhost:root/my-repo.git")
    assert found is some_proj


@patch("crucilab.helpers.requests.request", side_effect=mock_request)
def test_create_merge_request(request, gitlab_mock):
    mr_title = "A merge request"
    issue_key = "SOF-10"
    target_branch = "master"

    # Give one repo the matching git URL
    some_proj = gitlab_mock.projects.list.return_value[8]
    some_proj.ssh_url_to_repo = "git@localhost:root/my-repo.git"

    crucible = Crucible(DUMMY_CRUCIBLE_SERVER)

    jira_client = Mock()
    c = Crucilab(Mock(), jira_client, gitlab_mock, crucible)
    c.create_merge_request(
        issue_key, mr_title, "my-repo", issue_key, target_branch)

    some_proj.mergerequests.create.assert_called_once_with(
        data=dict(
            source_branch=issue_key, target_branch=target_branch,
            title=mr_title, remove_source_branch=True))

    jira_client.add_comment.assert_called_once_with(issue_key, ANY)


@patch("crucilab.helpers.requests.request", side_effect=mock_request)
def test_create_mr_nonexisting_project(request, gitlab_mock):
    issue_key = "SOF-10"

    crucible = Crucible(DUMMY_CRUCIBLE_SERVER)

    jira_client = Mock()
    c = Crucilab(Mock(), jira_client, gitlab_mock, crucible)
    with raises(AttributeError):
        c.create_merge_request(
            issue_key, "foo", "my-repo", issue_key, "master")
    # comment should be added in case of error
    jira_client.add_comment.assert_called_once_with(issue_key, ANY)


@patch("crucilab.helpers.requests.request", side_effect=mock_request)
def test_create_merge_request_for_issue(request, gitlab_mock):
    issue_key = "SOF-10"

    # Give one repo the matching git URL
    some_proj = gitlab_mock.projects.list.return_value[8]
    some_proj.ssh_url_to_repo = "git@localhost:root/my-repo.git"

    crucible = Crucible(DUMMY_CRUCIBLE_SERVER)
    jira_client = Mock()
    issue = jira_client.issue.return_value
    issue.fields.summary = "Do something"

    expected_title = "{}: {}".format(issue_key, issue.fields.summary)
    c = Crucilab(Mock(), jira_client, gitlab_mock, crucible)
    c.create_merge_requests_for_issue(issue_key)

    # We are expecting 6 merge requests.
    assert some_proj.mergerequests.create.call_count == 6
    some_proj.mergerequests.create.assert_has_calls(
        (
            call(data=dict(
                source_branch="SOF-10", target_branch="master",
                title=expected_title, remove_source_branch=True
            )),
            call(data=dict(
                source_branch="SOF-11", target_branch="master",
                title=expected_title, remove_source_branch=True
            )),
            call(data=dict(
                source_branch="SOF-12", target_branch="SOF-10",
                title=expected_title, remove_source_branch=True
            )),
            call(data=dict(
                source_branch="FOO-1", target_branch="master",
                title=expected_title, remove_source_branch=True
            )),
            call(data=dict(
                source_branch="FOO-2", target_branch="master",
                title=expected_title, remove_source_branch=True
            )),
            call(data=dict(
                source_branch="FOO-3", target_branch="SOF-10",
                title=expected_title, remove_source_branch=True
            )),
        ), any_order=True
    )
