from mock import patch

from crucilab.cli import main


@patch("crucilab.cli.start_server")
def test_main(start_server):
    env_file = "path/to/env"

    main(["--env_file", env_file])
    start_server.assert_called_once_with(env_file)
