from os.path import join
from os.path import dirname

AUX_DIR = join(dirname(__file__), "aux")


def get_path(file_name):
    return join(AUX_DIR, file_name)


def read(file_name):
    with open(get_path(file_name), "r") as f:
        return f.read()
