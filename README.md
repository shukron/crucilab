Overview
========

WebHooks server connecting JIRA, Crucible and GitLab.

### Target Audience

This server is meant for software teams that uses:
1. GitLab - for hosting code, repo management and performing CI.
2. JIRA - For task management.
3. Crucible - For code review.

#### Typical Workflow
1. Every line of code is written in the scope of a JIRA issue. This is where it 
all starts.
1. When a developer is assigned to the issue, he creates a _topic branch_ in
every repository that requires work in the scope of the issue.
1. When the developer feels like he has completed his task, he opens a
_Crucible Review_ that will group all the changes done in all the repositories
while working on the issue.
1. Reviewers will comment om the review, and fixes will be made.
1. When the reviewers are content with the result, they will close the review,
along with the jira issue.

#### The problem
But when and how the changes to the code are actually applied, and by who?  

When using Gitlab, the normal way to do this is by opening a _Merge Request_. 
Since the review might span across several repositories, that means opening
multiple merge request, and merging them when the review closes.
Currently there is no automatic way to open a Merge Request when a review
starts, and to merge it when the review closes.

**That's what this project is all about**
