FROM ubuntu:16.04

RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /app
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR /app/src
ENV PYTHONPATH=.
ENTRYPOINT ["python", "crucilab/cli.py"]
